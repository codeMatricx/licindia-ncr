<?php 
include "admin/database.php";
$sql ="SELECT name, mobile,image, heading from profile";
$result = $conn->query($sql);
if($result->num_rows > 0)
{
   $profile = $result->fetch_assoc();
}

 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Awards - LIC India NCR</title>
      <meta name="description" content="Your website description." />
      <meta name="keywords" content="other" />
      <meta name="generator" content="Parallels Web Presence Builder 11.0.14" />
      <link rel="stylesheet" type="text/css" href="css/style.css" />
      <!--[if IE 7]>
      <link rel="stylesheet" type="text/css" href="../../css/ie7.css?template=generic" />
      <![endif]-->
      <script language="JavaScript" type="text/javascript" src="js/style-fix.js"></script>
      <script language="JavaScript" type="text/javascript" src="js/css_browser_selector.js"></script>
      <link type="text/css" href="css/breadcrumbs-8685e8f3-5244-eb19-e021-18a5ed6b824a.css" rel="stylesheet" />
      <link type="text/css" href="css/header-9f0de22c-309d-01e2-c041-eb8e0648ccc6.css" rel="stylesheet" />
      <link type="text/css" href="css/navigation-617ca861-db53-45a1-27f0-a036f4a59f85.css" rel="stylesheet" />
      <script type="text/javascript">var addthis_config = {
         ui_language: 'en'
         };
      </script><script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js?ac=1501649269"></script>
      <script type="text/javascript">addthis.addEventListener('addthis.ready', function() {
         for (var i in addthis.links) {
         	var link = addthis.links[i];
         	if (link.className.indexOf("tweet") > -1) {
         		var iframe = link.firstChild;
         		if (iframe.src.indexOf("http://") !== 0) {
         			iframe.src = iframe.src.replace(/^(\/\/|https:\/\/)/, 'http://');
         		}
         	}
         }
         });
      </script><script type="text/javascript" src="components/jquery/jquery.js"></script>
      <script type="text/javascript" src="components/jquery/jquery.jcarousellite.js"></script>
      <script type="text/javascript" src="components/jquery/jquery.simplemodal.js"></script>
      <script type="text/javascript" src="modules/imagegallery/imagegallery.js"></script>
      <link type="text/css" href="modules/imagegallery/imagegallery.css" rel="stylesheet" />
      <script type="text/javascript" src="modules/imagegallery/imagegallery.locale-en_US.js"></script>
      <!--[if IE]>
      <style type="text/css">.background23-white{filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="../../images/background23-white.png?template=generic",sizingmethod=scale,enabled=true);} </style>
      <![endif]-->
      <style type="text/css">body{background-image: url("attachments/Background/asd.jpg");background-position: top left;background-repeat:repeat;}</style>
      <style type="text/css"></style>
      <!--[if IE]>
      <meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT" />
      <![endif]-->
      <link rel="shortcut icon" href="favicon.ico">
      <script type="text/javascript">var siteBuilderJs = jQuery.noConflict(true);</script><script type="text/javascript">siteBuilderJs(document).ready(function($){
         $("#widget-eb1185e6-5be9-4b90-c7c7-7227aa3582e2 .widget-content").imagegallery({
         	thumbSize: 'small',
         	pageSize: 1,
         	isShowFullSize: true,
         	isRtl: false,
         	openImageCallback: function(gallery) {
         		$('#imageGalleryFullSizeButton').click(function() {
         			window.open(gallery.currentImage.url);
         		});
         	},
         	store: {"type":"array","data":[{"id":"246ba7b9-a176-ff7f-8ebd-bad202f213ec","title":"Img2016","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/246ba7b9-a176-ff7f-8ebd-bad202f213ec.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/246ba7b9-a176-ff7f-8ebd-bad202f213ec.thumb.jpg"},{"id":"8ef98089-8794-416c-57be-c6d8821d4dd5","title":"Img2016 (6)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/8ef98089-8794-416c-57be-c6d8821d4dd5.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/8ef98089-8794-416c-57be-c6d8821d4dd5.thumb.jpg"},{"id":"b9d5db5a-bc62-2e05-3205-d0fb185eec68","title":"Img2016 (5)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/b9d5db5a-bc62-2e05-3205-d0fb185eec68.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/b9d5db5a-bc62-2e05-3205-d0fb185eec68.thumb.jpg"},{"id":"c11d2812-c926-fbef-b191-69b6e7781c1c","title":"Img2016 (4)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c11d2812-c926-fbef-b191-69b6e7781c1c.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c11d2812-c926-fbef-b191-69b6e7781c1c.thumb.jpg"},{"id":"cd2cea7d-a755-2f9e-e538-0dde71649187","title":"Img2016 (3)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/cd2cea7d-a755-2f9e-e538-0dde71649187.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/cd2cea7d-a755-2f9e-e538-0dde71649187.thumb.jpg"},{"id":"6ed561a6-e2c1-6d1e-899a-65c1d1a4c40e","title":"Img2016 (2)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/6ed561a6-e2c1-6d1e-899a-65c1d1a4c40e.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/6ed561a6-e2c1-6d1e-899a-65c1d1a4c40e.thumb.jpg"},{"id":"82f05f50-876a-5f84-61e1-397bf99b61df","title":"Img2016 (1)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/82f05f50-876a-5f84-61e1-397bf99b61df.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/82f05f50-876a-5f84-61e1-397bf99b61df.thumb.jpg"},{"id":"5b06fb35-8fb9-860e-3722-32686d7c9e11","title":"Ing2016","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5b06fb35-8fb9-860e-3722-32686d7c9e11.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5b06fb35-8fb9-860e-3722-32686d7c9e11.thumb.jpg"},{"id":"ac5d56d6-6e26-a5fc-79f4-fc246532ca22","title":"Ing2016 (1)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/ac5d56d6-6e26-a5fc-79f4-fc246532ca22.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/ac5d56d6-6e26-a5fc-79f4-fc246532ca22.thumb.jpg"},{"id":"76b05f87-f5cf-54a3-7a9b-078265f710d9","title":"Ing2016 (2)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/76b05f87-f5cf-54a3-7a9b-078265f710d9.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/76b05f87-f5cf-54a3-7a9b-078265f710d9.thumb.jpg"},{"id":"7101ac13-9b87-101f-7acc-36f2428a5c9e","title":"Ing2016 (3)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/7101ac13-9b87-101f-7acc-36f2428a5c9e.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/7101ac13-9b87-101f-7acc-36f2428a5c9e.thumb.jpg"},{"id":"0cb9eed4-d14d-e0d1-0c06-44bb3fce6927","title":"Ing2016 (4)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0cb9eed4-d14d-e0d1-0c06-44bb3fce6927.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0cb9eed4-d14d-e0d1-0c06-44bb3fce6927.thumb.jpg"},{"id":"9d13d374-6c23-f3bc-e672-90d8bbb5f081","title":"Ing2016 (5)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/9d13d374-6c23-f3bc-e672-90d8bbb5f081.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/9d13d374-6c23-f3bc-e672-90d8bbb5f081.thumb.jpg"},{"id":"022cbcb2-d086-efef-94df-7849b8e86b74","title":"Ing2016 (6)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/022cbcb2-d086-efef-94df-7849b8e86b74.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/022cbcb2-d086-efef-94df-7849b8e86b74.thumb.jpg"},{"id":"bff3f712-ded2-de83-f3d2-1506b9946af0","title":"Ing2016 (7)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/bff3f712-ded2-de83-f3d2-1506b9946af0.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/bff3f712-ded2-de83-f3d2-1506b9946af0.thumb.jpg"},{"id":"3a4d7046-1187-7f9e-1e49-390e2575e5ea","title":"Ing2016 (8)","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/3a4d7046-1187-7f9e-1e49-390e2575e5ea.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/3a4d7046-1187-7f9e-1e49-390e2575e5ea.thumb.jpg"},{"id":"ba2a374d-db0c-4685-49ef-2d4f4225380d","title":"26_01_2016_003_038","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/ba2a374d-db0c-4685-49ef-2d4f4225380d.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/ba2a374d-db0c-4685-49ef-2d4f4225380d.thumb.jpg"},{"id":"8e80095e-09f4-0f9e-1a75-c8a9c41e0981","title":"Retirement","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/8e80095e-09f4-0f9e-1a75-c8a9c41e0981.png","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/8e80095e-09f4-0f9e-1a75-c8a9c41e0981.thumb.png"},{"id":"4ec03889-5316-5174-18a2-2fc70511ac34","title":"14invest6","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/4ec03889-5316-5174-18a2-2fc70511ac34.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/4ec03889-5316-5174-18a2-2fc70511ac34.thumb.jpg"},{"id":"68706f46-f84d-3a49-7976-54f508f2afa9","title":"Why LIC","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/68706f46-f84d-3a49-7976-54f508f2afa9.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/68706f46-f84d-3a49-7976-54f508f2afa9.thumb.jpg"},{"id":"5dfbe34e-f0bc-0040-daf3-ec121815416b","title":"05pension1","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5dfbe34e-f0bc-0040-daf3-ec121815416b.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5dfbe34e-f0bc-0040-daf3-ec121815416b.thumb.jpg"},{"id":"0a708748-d5ee-19b6-67da-fcada1b752b4","title":"Angry-child","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a708748-d5ee-19b6-67da-fcada1b752b4.png","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a708748-d5ee-19b6-67da-fcada1b752b4.thumb.png"},{"id":"b5023b88-d0fc-8a50-0874-e702f0ac2a21","title":"Buymediclaim","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/b5023b88-d0fc-8a50-0874-e702f0ac2a21.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/b5023b88-d0fc-8a50-0874-e702f0ac2a21.thumb.jpg"},{"id":"2911b023-20a3-eae7-ce2f-33b07f2037a9","title":"Fp-circle","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/2911b023-20a3-eae7-ce2f-33b07f2037a9.png","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/2911b023-20a3-eae7-ce2f-33b07f2037a9.thumb.png"},{"id":"5604d999-115b-44d5-5f5b-20328ddfdc33","title":"Join-lic-career","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5604d999-115b-44d5-5f5b-20328ddfdc33.gif","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5604d999-115b-44d5-5f5b-20328ddfdc33.thumb.gif"},{"id":"0a0fdc4c-a4e7-7554-81fa-13f171657037","title":"LIC logo","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a0fdc4c-a4e7-7554-81fa-13f171657037.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a0fdc4c-a4e7-7554-81fa-13f171657037.thumb.jpg"},{"id":"0a2b3847-3eb8-178d-95cd-2d020a06c1e7","title":"Lic performance","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a2b3847-3eb8-178d-95cd-2d020a06c1e7.jpg","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a2b3847-3eb8-178d-95cd-2d020a06c1e7.thumb.jpg"},{"id":"80479b8d-af79-0480-192b-f460ee6fd9f0","title":"Licflag","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/80479b8d-af79-0480-192b-f460ee6fd9f0.gif","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/80479b8d-af79-0480-192b-f460ee6fd9f0.thumb.gif"},{"id":"c3378d0a-565b-fd2a-b37a-74fb9b1e62d4","title":"Nri","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c3378d0a-565b-fd2a-b37a-74fb9b1e62d4.png","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c3378d0a-565b-fd2a-b37a-74fb9b1e62d4.thumb.png"},{"id":"c1ab40f2-5d17-aa85-87bf-213614df79aa","title":"Retirement-advisor","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c1ab40f2-5d17-aa85-87bf-213614df79aa.png","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c1ab40f2-5d17-aa85-87bf-213614df79aa.thumb.png"},{"id":"072f49b1-ad9f-8b45-396f-22886b3a7b1c","title":"Think-center","description":"","url":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/072f49b1-ad9f-8b45-396f-22886b3a7b1c.png","thumbUrl":"..\/..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/072f49b1-ad9f-8b45-396f-22886b3a7b1c.thumb.png"}]}
         });
         });
      </script>
   </head>
   <body id="template" class="background-custom">
      <div class="site-frame">
         <div id="wrapper" class="container-content external-border3-black ">
            <!----<div class="external-top"><div><div><div><div></div></div></div></div></div> ---------->
            <div class="external-side-left">
               <div class="external-side-left-top">
                  <div class="external-side-left-bottom">
                     <div class="external-side-right">
                        <div class="external-side-right-top">
                           <div class="external-side-right-bottom">
                              <div id="watermark" class="pageContentText background23-white background border-none">
                                 <div id="watermark-content" class="container-content">
                                    <div id="layout">
                                       <div id="header" class="header border-none">
                                          <div id="header-top" class="top">
                                             <div>
                                                <div></div>
                                             </div>
                                          </div>
                                          <div id="header-side" class="side">
                                             <div id="header-side2" class="side2">
                                                <div class="container-content">
                                                   <div id="header-content">
                                                      <div class="container-content-inner" id="header-content-inner">
                                                         <!----<div class="widget widget-text" id="widget-fdc8ed2e-80aa-dad9-32a1-1f9e25f1a2ee">
                                                            <div class="widget-content"><p><img id="mce-6084" style="margin-right: auto; margin-left: auto; display: block;" src="../../attachments/Image/topheader_1.png?template=generic" alt="" width="1024" height="10" /></p></div>
                                                            </div> ---->
                                                         <div class="widget widget-header" id="widget-9f0de22c-309d-01e2-c041-eb8e0648ccc6">
                                                            <div class="widget-content">
                                                               <a href="home"><img src="attachments/Header/finallogo.png" class="header-image5" alt="" style="width:220px;height:94px;"></a>
                                                               <img src="admin/assets/img/profile/<?php echo isset($profile['image'])?$profile['image']:"image"; ?>" class="header-image1" alt="" style="width:140px;height:110px;">
                                                            <div class="header-image2">
                                                               <b> <?php echo isset($profile['heading'])?$profile['heading']:"heading"; ?></b>
                                                               <p> <?php echo isset($profile['name'])?$profile['name']:"name"; ?> <br> Mobile no. <?php echo isset($profile['mobile'])?$profile['mobile']:"mobile"; ?></p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="widget widget-navigation" id="widget-617ca861-db53-45a1-27f0-a036f4a59f85">
                                                            <div class="widget-content">
                                                               <ul class="navigation" id="navigation-617ca861-db53-45a1-27f0-a036f4a59f85">
                                                                  <li class="selected">
                                                                     <a href="home">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Home</span>
                                                                     </a>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="about-lic">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">About LIC</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class="">
                                                                           <a href="history">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">History</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="objective">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Objective</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://www.licindia.in/Media/Awards.aspx">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Awards</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="opreations">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Operations</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="subsidiaries">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Subsidiaries</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="performance">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Performance</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="lic-act-1956">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">LIC Act 1956</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="about-us">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">About Us</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class="">
                                                                           <a href="our-vision">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Our Vision</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="our-mission">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Our Mission</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="our-team">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Our Team</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="infopage">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">InfoPage</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class="">
                                                                           <a href="tax-planning">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Tax Planning</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Customer-Services/Bonus-Information">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Bonus Information</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Customer-Services/Phone-Help-Line">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">LIC Helpline Numbers</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="financial-planning">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Financial Planning</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class="">
                                                                           <a href="life-cycle">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Life Cycle</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="page">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Why</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="when">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">When</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="how">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">How</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="taxation">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">TAXATION</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="#">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Lic Products</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class=" navigation-item-expand">
                                                                  <a href="#">
                                                                  <span class="navigation-item-bullet">&gt;</span>
                                                                  <span class="navigation-item-text">New Policies</span>
                                                                  </a>
                                                                     <ul>
                                                                     <?php 
                                                                        $sql = "SELECT * FROM licproduct";
                                                                        $result = $conn->query($sql);
                                                                        if($result->num_rows > 0)
                                                                        {
                                                                           while($licproduct = $result->fetch_assoc())
                                                                           {
                                                                              ?>
                                                                              <li class="">
                                                                              <a href="<?php echo $licproduct['link']; ?>">
                                                                              <span class="navigation-item-bullet">&gt;</span>
                                                                              <span class="navigation-item-text"><?php echo $licproduct['name']; ?></span>
                                                                              </a>
                                                                           </li>
                                                                           <?php
                                                                           }
                                                                        }
                                                                      ?>
                                                                      </ul>
                                                                      </li>
                                                                     <li class="">
                                                                        <a href="http://licindia.in/Products/Aam-Aadmi-Bima-Yojana">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">Aam Aadmi Bima Yojna</span>
                                                                        </a>
                                                                     </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Insurance-Plan">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Insurance Plan</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Special-Plans">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Special Plans</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Pension-Plans">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Pension Plan</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Unit-Plans">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Unit Plans</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Micro-Insurance-Plans">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Micro Insurance Plans</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Withdrawn-Plans">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Withdrawn Plans</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Health-Plans">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Health plans</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="#">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Services</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class="">
                                                                           <a href="http://www.licindia.in/Customer-Services/Tax-Benefit">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Tax-Benefit</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Home-(1)/LICOnlineServicePortal">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Get Policy Status</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://www.licindia.in/Customer-Services/Premium-Calculator.aspx">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Premium Calculator</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="contact">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Premium Payments</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="faqs">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">FAQs</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Bottom-Links/Download-Forms">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Download Form</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="#" >
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Utilities</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class="">
                                                                           <a href="attachments/gscop.pdf" target="_blank">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">About GS Cop</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="our-product">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Products</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class="">
                                                                     <a href="achievements">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Achievements</span>
                                                                     </a>
                                                                  </li>
                                                                  <li class="">
                                                                     <a href="contact">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Contact Us</span>
                                                                     </a>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="header-bottom" class="bottom">
                                             <div>
                                                <div></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="columns">
                                          <div id="column2" class="column2 column column-right border-none">
                                             <div id="column2-top" class="top">
                                                <div>
                                                   <div></div>
                                                </div>
                                             </div>
                                             <div id="column2-side" class="side">
                                                <div id="column2-side2" class="side2">
                                                   <div class="container-content">
                                                      <div id="column2-content">
                                                         <div class="container-content-inner" id="column2-content-inner">
                                                            <div class="widget widget-text" id="widget-feb2993b-a7e5-da27-3e85-80ec89145ac3">
                                                               <div class="widget-content">
                                                                  <p><a class=" link" href="#"><img id="mce-3946" style="margin-left: auto; margin-right: auto; display: block;" src="attachments/Image/tax11.png" alt="" width="180" height="108" /></a></p>
                                                                  <hr />
                                                               </div>
                                                            </div>
                                                            <div id="widget-3e5c4e66-2826-ed09-0e62-1cdc2a5a1385" class="widget widget-sharethis">
                                                               <h2 class="widget-title">Social Share...</h2>
                                                               <div class="widget-content">
                                                                  <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                                                     <a class="addthis_button_facebook"></a>
                                                                     <a class="addthis_button_twitter"></a>
                                                                     <a class="addthis_button_preferred_1"></a>
                                                                     <a class="addthis_button_preferred_2"></a>
                                                                     <a class="addthis_button_compact"></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="widget widget-text" id="widget-95f4b402-4ab8-f388-1e9a-8dd4e289c907">
                                                               <div class="widget-content">
                                                                  <hr />
                                                                  <p><a class=" link" href="#"><img id="mce-7805" style="margin-left: auto; margin-right: auto; display: block;" title="Invest now and retire rich" src="attachments/Image/invest-now-button_1.jpg" alt="Invest now and retire rich" width="180" height="65" /></a></p>
                                                                  <hr />
                                                               </div>
                                                            </div>
                                                            <div id="widget-6bf1c9ee-9244-65b9-200d-56fb1e35f650" class="widget widget-pagecontent">
                                                               <div class="widget-content">
                                                                  <div class="widget widget-text" id="widget-86eb1559-5207-22c0-5900-936f6611a172">
                                                                     <div class="widget-content">
                                                                        <p style="text-align: center;"><span style="font-weight: bold; font-size: 15px;"><a class="link" name="forform"></a>QUERY SUBMISSION</span></p>
                                                                     </div>
                                                                  </div>
                                                                  <div id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed" class="widget widget-contact">
                                                                     <div class="widget-content">
                                                                        <form id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-form" class="ajax-form2" action="php/querymail.php" method="post">
                                                                           <div class="form-item"><label class="form-item-label" for="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-name">Name <span class="form-required">*</span></label>
                                                                              <input type="text" id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-name" name="name" class="form-item-textfield required" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label" for="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-field1">Mobile <span class="form-required">*</span></label>
                                                                              <input type="text" id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-field1" name="field1" class="form-item-textfield required" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label" for="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-field2">Your Address</label>
                                                                              <input type="text" id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-field2" name="field2" class="form-item-textfield" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label" for="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-mail">E-mail <span class="form-required">*</span></label>
                                                                              <input type="text" id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-mail" name="mail" class="form-item-textfield required email" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label" for="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-message">Your Query <span class="form-required">*</span></label><textarea id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-message" name="message" class="form-item-textarea expand100-200 required"></textarea>
                                                                           </div>
                                                                           <div class="form-note">Fields marked with <span class="form-required">*</span> are required.</div>
                                                                           <input type="submit" value="Send e-mail">
                                                                        </form>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div id="widget-eb1185e6-5be9-4b90-c7c7-7227aa3582e2" class="widget widget-imagegallery">
                                                               <h2 class="widget-title">Must View...</h2>
                                                               <div class="widget-content">
                                                                  <noscript>
                                                                     <div class="noscript-warning">
                                                                        <div class="noscript-warning-inner">
                                                                           <div class="noscript-warning-icon">JavaScript support is not enabled in your browser. To see the contents of this page, you need to enable JavaScript. Click <a href='http://www.google.com/support/bin/answer.py?answer=23852' target='_blank'>here</a> to learn how to enable JavaScript.</div>
                                                                        </div>
                                                                     </div>
                                                                  </noscript>
                                                               </div>
                                                            </div>
                                                            <div class="widget widget-text" id="widget-7bed8a8f-5fef-443f-80ba-9a977f3e4146">
                                                               <div class="widget-content">
                                                                  <p><a class=" link" href="contact.php"><img id="mce-51739" style="float: left; margin-right: 20px;" src="attachments/Image/fix-appointment.jpg" alt="" width="180" height="64" /></a></p>
                                                                  <p>&nbsp;</p>
                                                                  <hr />
                                                               </div>
                                                            </div>
                                                            <div class="widget widget-text" id="widget-b51ca795-8e8a-4a55-a998-ad61f982491f">
                                                               <div class="widget-content">
                                                                  <p style="text-align: center;"><a class=" link" href="#"><span style="font-weight: bold;">Why Consider Us</span></a></p>
                                                                  <!---<p style="text-align: center;"><a class=" link" href="#"><img id="mce-7485" style="margin-left: auto; margin-right: auto; display: block;" src="attachments/Image/financial-advisor-for-you.gif" alt="" width="53" height="100" /></a></p>--->
                                                                  <hr />
                                                               </div>
                                                            </div>
                                                            <div id="widget-6bf1c9ee-9244-65b9-200d-56fb1e35f650" class="widget widget-pagecontent">
                                                               <div class="widget-content">
                                                                  <div id="widget-28d6e5bc-1130-45e5-2dd2-a65ca8052f62" class="widget widget-sharethis">
                                                                     <h2 class="widget-title">Let Others Know.</h2>
                                                                     <div class="widget-content"><a class="addthis_button"></a></div>
                                                                  </div>
                                                                  <div class="widget widget-text" id="widget-ccb325d6-420f-8f00-965b-56e6fd667e6b">
                                                                     <div class="widget-content"></div>
                                                                  </div>
                                                                  <div class="widget widget-script" id="widget-2a2603ff-d9a6-86ac-700f-0b37a5cdc609">
                                                                     <!---<div class="widget-content"><script language=JavaScript>
                                                                        var message="Sorry! Right Click Disabled";
                                                                        function clickIE4(){
                                                                        if (event.button==2){
                                                                        alert(message);
                                                                        return false;
                                                                        }}
                                                                        function clickNS4(e){
                                                                        if (document.layers||document.getElementById&&!document.all){
                                                                        if (e.which==2||e.which==3){
                                                                        alert(message);
                                                                        return false;
                                                                        }}}
                                                                        if (document.layers){
                                                                        document.captureEvents(Event.MOUSEDOWN);
                                                                        document.onmousedown=clickNS4;
                                                                        }
                                                                        else if (document.all&&!document.getElementById){
                                                                        document.onmousedown=clickIE4;
                                                                        }
                                                                        document.oncontextmenu=new Function("alert(message);return false")
                                                                        </script></div>--->
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div id="column2-bottom" class="bottom">
                                                <div>
                                                   <div></div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="content" class="content border-none">
                                             <div id="content-top" class="top">
                                                <div>
                                                   <div></div>
                                                </div>
                                             </div>
                                             <div id="content-side" class="side">
                                                <div id="content-side2" class="side2">
                                                   <div class="container-content">
                                                      <div id="content-content">
                                                         <div class="container-content-inner" id="content-content-inner">
                                                            <div class="widget widget-breadcrumbs" id="widget-8685e8f3-5244-eb19-e021-18a5ed6b824a">
                                                               <div class="widget-content">
                                                                  <a class="icon-with-title" href="home"></a>
                                                                  <div itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="page" href="home.html" itemprop="url"><span itemprop="title">Start</span></a><span class="separator">→</span></div>
                                                                  <div itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="page" href="about-lic.html" itemprop="url"><span itemprop="title">About LIC</span></a><span class="separator">→</span></div>
                                                                  <div itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title" class="page">Awards</span></div>
                                                               </div>
                                                            </div>
                                                            <div id="widget-b33647b2-12d5-caf1-a237-bfe08d5a9f06" class="widget widget-pagecontent">
                                                               <div class="widget-content">
                                                                  <div class="widget widget-text" id="widget-4623da79-9c80-266f-2061-7807b91c4202">
                                                                     <div class="widget-content">
                                                                        <hr />
                                                                        <p>&nbsp;<img id="mce-3600" style="float: left; margin-right: 20px;" src="attachments/Image/awards_1.png" alt="" width="511" height="19" /></p>
                                                                     </div>
                                                                  </div>
                                                                  <div class="widget widget-script" id="widget-941f0b9e-11a0-59c8-2171-b2bba72fda15">
                                                                     <div class="widget-content"><IFRAME SRC="http://www.licindia.in/Media/Awards" width="800px" height="900px" id="iframe2" marginheight="0" frameborder="0" onLoad="autoResize('iframe2');"  align="middle" frameborder="0" scrolling="auto" seamless ></iframe></div>
                                                                  </div>
                                                                  <div class="widget widget-text" id="widget-530d3436-4bf4-c6c1-9825-e9017237ee7f">
                                                                     <div class="widget-content">
                                                                        <hr />
                                                                     </div>
                                                                  </div>
                                                                  <div class="widget widget-text" id="widget-eb613f8d-ac8e-926b-b959-cb4d0796f1ad">
                                                                     <div class="widget-content">
                                                                        <p style="text-align: justify;"><span style="font-size: 20px; font-weight: bold;">Know About Your Life Insurance</span><br /><br /><span style="font-size: 16px; font-weight: bold;">Life insurance in India made its debut well over 100 years ago.</span><br /><br /><span style="font-size: 16px;">In our country, which is one of the most populated in the world, the prominence of insurance is not as widely understood, as it ought to be. What follows is an attempt to acquaint readers with some of the concepts of life insurance, with special reference to LIC.</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">It should, however, be clearly understood that the following content is by no means an exhaustive description of the terms and conditions of an LIC policy or its benefits or privileges.</span><br /><br /><span style="font-size: 16px;">For more details, please contact our branch or divisional office. Any LIC Agent will be glad to help you choose the life insurance plan to meet your needs and render policy servicing.</span><br /><span style="font-size: 16px;">&nbsp;</span><br /><span style="font-weight: bold; font-size: 16px;">What Is Life Insurance?</span><br /><br /><span style="font-size: 16px;">Life insurance is a contract that pledges payment of an amount to the person assured (or his nominee) on the happening of the event insured against.</span><br /><br /><span style="font-size: 16px;">The contract is valid for payment of the insured amount during:</span><br /><span style="font-size: 16px;">&raquo; The date of maturity, or</span><br /><span style="font-size: 16px;">&raquo; Specified dates at periodic intervals, or</span><br /><span style="font-size: 16px;">&raquo; Unfortunate death, if it occurs earlier.</span><br /><br /><span style="font-size: 16px;">Among other things, the contract also provides for the payment of premium periodically to the Corporation by the policyholder. Life insurance is universally acknowledged to be an institution, which eliminates 'risk', substituting certainty for uncertainty and comes to the timely aid of the family in the unfortunate event of death of the breadwinner. &nbsp;</span><br /><br /><span style="font-size: 16px;">By and large, life insurance is civilisation's partial solution to the problems caused by death. Life insurance, in short, is concerned with two hazards that stand across the life-path of every person:</span><br /><br /><span style="font-size: 16px;">1.That of dying prematurely leaving a dependent family to fend for itself.</span><br /><span style="font-size: 16px;">2.That of living till old age without visible means of support.</span><br /><span style="font-size: 16px;">&nbsp;</span><br /><span style="font-size: 20px; font-weight: bold;">Life Insurance Vs. Other Savings</span><br /><br /><span style="font-size: 16px; font-weight: bold;">Contract Of Insurance:</span><br /><span style="font-size: 16px;">A contract of insurance is a contract of utmost good faith technically known as uberrima fides. The doctrine of disclosing all material facts is embodied in this important principle, which applies to all forms of insurance.</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">At the time of taking a policy, policyholder should ensure that all questions in the proposal form are correctly answered. Any misrepresentation, non-disclosure or fraud in any document leading to the acceptance of the risk would render the insurance contract null and void.</span><br /><br /><span style="font-size: 16px; font-weight: bold;">Protection:</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">Savings through life insurance guarantee full protection against risk of death of the saver. Also, in case of demise, life insurance assures payment of the entire amount assured (with bonuses wherever applicable) whereas in other savings schemes, only the amount saved (with interest) is payable.</span><br /><br /><span style="font-size: 16px; font-weight: bold;">Aid To Thrift:</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">Life insurance encourages 'thrift'. It allows long-term savings since payments can be made effortlessly because of the 'easy instalment' facility built into the scheme. (Premium payment for insurance is either monthly, quarterly, half yearly or yearly).</span><br /><span style="font-size: 16px;"><span style="text-decoration: underline;">For example:</span> The Salary Saving Scheme popularly known as SSS, provides a convenient method of paying premium each month by deduction from one's salary.</span><br /><span style="font-size: 16px;">In this case the employer directly pays the deducted premium to LIC. The Salary Saving Scheme is ideal for any institution or establishment subject to specified terms and conditions.</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px; font-weight: bold;">Liquidity:</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">In case of insurance, it is easy to acquire loans on the sole security of any policy that has acquired loan value. Besides, a life insurance policy is also generally accepted as security, even for a commercial loan.</span><br /><br /><span style="font-size: 16px; font-weight: bold;">Tax Relief:</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">Life Insurance is the best way to enjoy tax deductions on income tax and wealth tax. This is available for amounts paid by way of premium for life insurance subject to income tax rates in force.</span><br /><span style="font-size: 16px;">Assessees can also avail of provisions in the law for tax relief. In such cases the assured in effect pays a lower premium for insurance than otherwise.</span><br /><br /><span style="font-size: 16px; font-weight: bold;">Money When You Need It:</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">A policy that has a suitable insurance plan or a combination of different plans can be effectively used to meet certain monetary needs that may arise from time-to-time.</span><br /><span style="font-size: 16px;">Children's education, start-in-life or marriage provision or even periodical needs for cash over a stretch of time can be less stressful with the help of these policies.</span><br /><span style="font-size: 16px;">Alternatively, policy money can be made available at the time of one's retirement from service and used for any specific purpose, such as, purchase of a house or for other investments. Also, loans are granted to policyholders for house building or for purchase of flats (subject to certain conditions).</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px; font-weight: bold;">Who Can Buy A Policy?</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">Any person who has attained majority and is eligible to enter into a valid contract can insure himself/herself and those in whom he/she has insurable interest.</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">Policies can also be taken, subject to certain conditions, on the life of one's spouse or children. While underwriting proposals, certain factors such as the policyholder&rsquo;s state of health, the proponent's income and other relevant factors are considered by the Corporation.</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px; font-weight: bold;">Insurance For Women</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">Prior to nationalisation (1956), many private insurance companies would offer insurance to female lives with some extra premium or on restrictive conditions. However, after nationalisation of life insurance, the terms under which life insurance is granted to female lives have been reviewed from time-to-time.</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">At present, women who work and earn an income are treated at par with men. In other cases, a restrictive clause is imposed, only if the age of the female is up to 30 years and if she does not have an income attracting Income Tax.</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px; font-weight: bold;">Medical And Non-Medical Schemes</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">Life insurance is normally offered after a medical examination of the life to be assured. However, to facilitate greater spread of insurance and also to avoid inconvenience, LIC has been extending insurance cover without any medical examination, subject to certain conditions.</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px; font-weight: bold;"><br /></span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px; font-weight: bold;">With Profit And Without Profit Plans</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">An insurance policy can be 'with' or 'without' profit. In the former, bonuses disclosed, if any, after periodical valuations are allotted to the policy and are payable along with the contracted amount.</span><br /><br /><span style="font-size: 16px;">In 'without' profit plan the contracted amount is paid without any addition. The premium rate charged for a 'with' profit policy is therefore higher than for a 'without' profit policy.</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px; font-weight: bold;">Keyman Insurance</span></p>
                                                                        <p style="text-align: justify;"><span style="font-size: 16px;">Keyman insurance is taken by a business firm on the life of key employee(s) to protect the firm against financial losses, which may occur due to the premature demise of the Keyman.</span><span style="font-size: 16px;"> <br /></span></p>
                                                                        <div id="balloon_parent_div_ab" class="kisb" style="visibility: hidden; position: absolute; left: 232px; top: 300px; text-align: justify;">
                                                                           <div class="kl_abmenu"><span style="font-size: 16px;">Add to Anti-Banner</span></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="widget widget-advertisement" id="widget-955e146a-998f-7a7c-2e6d-690d08a72a9a">
                                                               <div class="widget-content">
                                                                  <SCRIPT charset="utf-8" type="text/javascript" src="http://ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Flic0f-20%2F8005%2F406d4897-f2eb-4057-a59f-3abfaf6da1da"> </SCRIPT> 
                                                                  <NOSCRIPT><A HREF="http://ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Flic0f-20%2F8005%2F406d4897-f2eb-4057-a59f-3abfaf6da1da&Operation=NoScript">Amazon.com Widgets</A></NOSCRIPT>
                                                               </div>
                                                            </div>
                                                            <!----<div class="widget widget-script" id="widget-a9b07752-923e-3c64-cfdf-461a7a1a9f1f">
                                                               <div class="widget-content"><script language=JavaScript> var message="Function Disabled!"; function clickIE4(){ if (event.button==2){ alert(message); return false; } } function clickNS4(e){ if (document.layers||document.getElementById&&!document.all){ if (e.which==2||e.which==3){ alert(message); return false; } } } if (document.layers){ document.captureEvents(Event.MOUSEDOWN); document.onmousedown=clickNS4; } else if (document.all&&!document.getElementById){ document.onmousedown=clickIE4; } document.oncontextmenu=new Function("alert(message);return false") </script>
                                                               </div>
                                                               </div>--->
                                                            <div class="widget widget-script" id="widget-28499317-295c-bb2a-ee38-4d8685ebf8c0">
                                                               <div class="widget-content">
                                                                  <script language="JavaScript1.2">
                                                                     //Disable select-text script (IE4+, NS6+)- By Andy Scott
                                                                     //Exclusive permission granted to Dynamic Drive to feature script
                                                                     //Visit http://www.dynamicdrive.com for this script
                                                                      
                                                                     function disableselect(e){
                                                                     return false
                                                                     }
                                                                      
                                                                     function reEnable(){
                                                                     return true
                                                                     }
                                                                      
                                                                     //if IE4+
                                                                     document.onselectstart=new Function ("return false")
                                                                      
                                                                     //if NS6
                                                                     if (window.sidebar){
                                                                     document.onmousedown=disableselect
                                                                     document.onclick=reEnable
                                                                     }
                                                                  </script> 
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div id="content-bottom" class="bottom">
                                                <div>
                                                   <div></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="footer-wrapper">
                                       <div id="footer" class="footer border-none">
                                          <div id="footer-top" class="top">
                                             <div>
                                                <div></div>
                                             </div>
                                          </div>
                                          <div id="footer-side" class="side">
                                             <div id="footer-side2" class="side2">
                                                <div class="container-content">
                                                   <div id="footer-content">
                                                      <div class="container-content-inner" id="footer-content-inner">
                                                         <div class="widget widget-text" id="widget-73fdf89a-3b66-ffa8-7965-cab511a338e3">
                                                            <div class="widget-content">
                                                               <p style="text-align: center;">&copy; 1989-2017. LIC India NCR . All Rights Reserved.</p>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="footer-bottom" class="bottom">
                                             <div>
                                                <div></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="external-bottom">
               <div>
                  <div>
                     <div>
                        <div></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript" src="../../js/anti_cache.js?ac=1501649269"></script>
      <script type="text/javascript">
         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', 'UA-34705881-1']);
         _gaq.push(['_trackPageview']);
         
         (function() {
           var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
           ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
           var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();
         
      </script>
   </body>
</html>