<?php 
include "admin/database.php";
$sql ="SELECT name, mobile,image, heading from profile";
$result = $conn->query($sql);
if($result->num_rows > 0)
{
   $profile = $result->fetch_assoc();
}

 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Contact Us - LIC India NCR</title>
      <meta name="description" content="Your website description." />
      <meta name="keywords" content="other" />
      <meta name="generator" content="Parallels Web Presence Builder 11.0.14" />
      <link rel="stylesheet" type="text/css" href="css/style.css" />
      <!--[if IE 7]>
      <link rel="stylesheet" type="text/css" href="../css/ie7.css?template=generic" />
      <![endif]-->
      <script language="JavaScript" type="text/javascript" src="js/style-fix.js"></script>
      <script language="JavaScript" type="text/javascript" src="js/css_browser_selector.js"></script>
      <link type="text/css" href="css/breadcrumbs-8685e8f3-5244-eb19-e021-18a5ed6b824a.css" rel="stylesheet" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link href="css/bootstrap/bootstrap.css" rel="stylesheet" />
      <link type="text/css" href="css/header-9f0de22c-309d-01e2-c041-eb8e0648ccc6.css" rel="stylesheet" />
      <link type="text/css" href="css/navigation-617ca861-db53-45a1-27f0-a036f4a59f85.css" rel="stylesheet" />
      <script type="text/javascript">var addthis_config = {
         ui_language: 'en'
         };
      </script><script type="text/javascript" src="js/250/addthis_widget.js"></script>
      <script type="text/javascript">addthis.addEventListener('addthis.ready', function() {
         for (var i in addthis.links) {
         	var link = addthis.links[i];
         	if (link.className.indexOf("tweet") > -1) {
         		var iframe = link.firstChild;
         		if (iframe.src.indexOf("http://") !== 0) {
         			iframe.src = iframe.src.replace(/^(\/\/|https:\/\/)/, 'http://');
         		}
         	}
         }
         });
      </script><script type="text/javascript" src="components/jquery/jquery.js"></script>
      <script type="text/javascript" src="components/jquery/jquery.jcarousellite.js"></script>
      <script type="text/javascript" src="components/jquery/jquery.simplemodal.js"></script>
      <script type="text/javascript" src="modules/imagegallery/imagegallery.js"></script>
      <link type="text/css" href="modules/imagegallery/imagegallery.css" rel="stylesheet" />
      <script type="text/javascript" src="modules/imagegallery/imagegallery.locale-en_US.js"></script>
      <!--[if IE]>
      <style type="text/css">.background23-white{filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="../images/background23-white.png?template=generic",sizingmethod=scale,enabled=true);} </style>
      <![endif]-->   
      <!--<style type="text/css">
         body{background-image: url("attachments/Background/asd.jpg");
         background-position: top left;
         background-repeat:repeat;}
         
         </style>-->
      <style type="text/css"></style>
      <!--[if IE]>
      <meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT" />
      <![endif]-->
      <link rel="shortcut icon" href="favicon.ico">
      <script type="text/javascript">var siteBuilderJs = jQuery.noConflict(true);</script><script type="text/javascript">siteBuilderJs(document).ready(function($){
         $("#widget-eb1185e6-5be9-4b90-c7c7-7227aa3582e2 .widget-content").imagegallery({
         	thumbSize: 'small',
         	pageSize: 1,
         	isShowFullSize: true,
         	isRtl: false,
         	openImageCallback: function(gallery) {
         		$('#imageGalleryFullSizeButton').click(function() {
         			window.open(gallery.currentImage.url);
         		});
         	},
         	store: {"type":"array","data":[{"id":"246ba7b9-a176-ff7f-8ebd-bad202f213ec","title":"Img2016","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/246ba7b9-a176-ff7f-8ebd-bad202f213ec.jpg","thumbUrl":"data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/246ba7b9-a176-ff7f-8ebd-bad202f213ec.thumb.jpg"},{"id":"8ef98089-8794-416c-57be-c6d8821d4dd5","title":"Img2016 (6)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/8ef98089-8794-416c-57be-c6d8821d4dd5.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/8ef98089-8794-416c-57be-c6d8821d4dd5.thumb.jpg"},{"id":"b9d5db5a-bc62-2e05-3205-d0fb185eec68","title":"Img2016 (5)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/b9d5db5a-bc62-2e05-3205-d0fb185eec68.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/b9d5db5a-bc62-2e05-3205-d0fb185eec68.thumb.jpg"},{"id":"c11d2812-c926-fbef-b191-69b6e7781c1c","title":"Img2016 (4)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c11d2812-c926-fbef-b191-69b6e7781c1c.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c11d2812-c926-fbef-b191-69b6e7781c1c.thumb.jpg"},{"id":"cd2cea7d-a755-2f9e-e538-0dde71649187","title":"Img2016 (3)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/cd2cea7d-a755-2f9e-e538-0dde71649187.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/cd2cea7d-a755-2f9e-e538-0dde71649187.thumb.jpg"},{"id":"6ed561a6-e2c1-6d1e-899a-65c1d1a4c40e","title":"Img2016 (2)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/6ed561a6-e2c1-6d1e-899a-65c1d1a4c40e.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/6ed561a6-e2c1-6d1e-899a-65c1d1a4c40e.thumb.jpg"},{"id":"82f05f50-876a-5f84-61e1-397bf99b61df","title":"Img2016 (1)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/82f05f50-876a-5f84-61e1-397bf99b61df.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/82f05f50-876a-5f84-61e1-397bf99b61df.thumb.jpg"},{"id":"5b06fb35-8fb9-860e-3722-32686d7c9e11","title":"Ing2016","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5b06fb35-8fb9-860e-3722-32686d7c9e11.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5b06fb35-8fb9-860e-3722-32686d7c9e11.thumb.jpg"},{"id":"ac5d56d6-6e26-a5fc-79f4-fc246532ca22","title":"Ing2016 (1)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/ac5d56d6-6e26-a5fc-79f4-fc246532ca22.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/ac5d56d6-6e26-a5fc-79f4-fc246532ca22.thumb.jpg"},{"id":"76b05f87-f5cf-54a3-7a9b-078265f710d9","title":"Ing2016 (2)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/76b05f87-f5cf-54a3-7a9b-078265f710d9.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/76b05f87-f5cf-54a3-7a9b-078265f710d9.thumb.jpg"},{"id":"7101ac13-9b87-101f-7acc-36f2428a5c9e","title":"Ing2016 (3)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/7101ac13-9b87-101f-7acc-36f2428a5c9e.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/7101ac13-9b87-101f-7acc-36f2428a5c9e.thumb.jpg"},{"id":"0cb9eed4-d14d-e0d1-0c06-44bb3fce6927","title":"Ing2016 (4)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0cb9eed4-d14d-e0d1-0c06-44bb3fce6927.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0cb9eed4-d14d-e0d1-0c06-44bb3fce6927.thumb.jpg"},{"id":"9d13d374-6c23-f3bc-e672-90d8bbb5f081","title":"Ing2016 (5)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/9d13d374-6c23-f3bc-e672-90d8bbb5f081.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/9d13d374-6c23-f3bc-e672-90d8bbb5f081.thumb.jpg"},{"id":"022cbcb2-d086-efef-94df-7849b8e86b74","title":"Ing2016 (6)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/022cbcb2-d086-efef-94df-7849b8e86b74.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/022cbcb2-d086-efef-94df-7849b8e86b74.thumb.jpg"},{"id":"bff3f712-ded2-de83-f3d2-1506b9946af0","title":"Ing2016 (7)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/bff3f712-ded2-de83-f3d2-1506b9946af0.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/bff3f712-ded2-de83-f3d2-1506b9946af0.thumb.jpg"},{"id":"3a4d7046-1187-7f9e-1e49-390e2575e5ea","title":"Ing2016 (8)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/3a4d7046-1187-7f9e-1e49-390e2575e5ea.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/3a4d7046-1187-7f9e-1e49-390e2575e5ea.thumb.jpg"},{"id":"ba2a374d-db0c-4685-49ef-2d4f4225380d","title":"26_01_2016_003_038","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/ba2a374d-db0c-4685-49ef-2d4f4225380d.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/ba2a374d-db0c-4685-49ef-2d4f4225380d.thumb.jpg"},{"id":"8e80095e-09f4-0f9e-1a75-c8a9c41e0981","title":"Retirement","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/8e80095e-09f4-0f9e-1a75-c8a9c41e0981.png","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/8e80095e-09f4-0f9e-1a75-c8a9c41e0981.thumb.png"},{"id":"4ec03889-5316-5174-18a2-2fc70511ac34","title":"14invest6","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/4ec03889-5316-5174-18a2-2fc70511ac34.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/4ec03889-5316-5174-18a2-2fc70511ac34.thumb.jpg"},{"id":"68706f46-f84d-3a49-7976-54f508f2afa9","title":"Why LIC","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/68706f46-f84d-3a49-7976-54f508f2afa9.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/68706f46-f84d-3a49-7976-54f508f2afa9.thumb.jpg"},{"id":"5dfbe34e-f0bc-0040-daf3-ec121815416b","title":"05pension1","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5dfbe34e-f0bc-0040-daf3-ec121815416b.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5dfbe34e-f0bc-0040-daf3-ec121815416b.thumb.jpg"},{"id":"0a708748-d5ee-19b6-67da-fcada1b752b4","title":"Angry-child","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a708748-d5ee-19b6-67da-fcada1b752b4.png","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a708748-d5ee-19b6-67da-fcada1b752b4.thumb.png"},{"id":"b5023b88-d0fc-8a50-0874-e702f0ac2a21","title":"Buymediclaim","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/b5023b88-d0fc-8a50-0874-e702f0ac2a21.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/b5023b88-d0fc-8a50-0874-e702f0ac2a21.thumb.jpg"},{"id":"2911b023-20a3-eae7-ce2f-33b07f2037a9","title":"Fp-circle","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/2911b023-20a3-eae7-ce2f-33b07f2037a9.png","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/2911b023-20a3-eae7-ce2f-33b07f2037a9.thumb.png"},{"id":"5604d999-115b-44d5-5f5b-20328ddfdc33","title":"Join-lic-career","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5604d999-115b-44d5-5f5b-20328ddfdc33.gif","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5604d999-115b-44d5-5f5b-20328ddfdc33.thumb.gif"},{"id":"0a0fdc4c-a4e7-7554-81fa-13f171657037","title":"LIC logo","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a0fdc4c-a4e7-7554-81fa-13f171657037.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a0fdc4c-a4e7-7554-81fa-13f171657037.thumb.jpg"},{"id":"0a2b3847-3eb8-178d-95cd-2d020a06c1e7","title":"Lic performance","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a2b3847-3eb8-178d-95cd-2d020a06c1e7.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a2b3847-3eb8-178d-95cd-2d020a06c1e7.thumb.jpg"},{"id":"80479b8d-af79-0480-192b-f460ee6fd9f0","title":"Licflag","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/80479b8d-af79-0480-192b-f460ee6fd9f0.gif","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/80479b8d-af79-0480-192b-f460ee6fd9f0.thumb.gif"},{"id":"c3378d0a-565b-fd2a-b37a-74fb9b1e62d4","title":"Nri","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c3378d0a-565b-fd2a-b37a-74fb9b1e62d4.png","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c3378d0a-565b-fd2a-b37a-74fb9b1e62d4.thumb.png"},{"id":"c1ab40f2-5d17-aa85-87bf-213614df79aa","title":"Retirement-advisor","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c1ab40f2-5d17-aa85-87bf-213614df79aa.png","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c1ab40f2-5d17-aa85-87bf-213614df79aa.thumb.png"},{"id":"072f49b1-ad9f-8b45-396f-22886b3a7b1c","title":"Think-center","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/072f49b1-ad9f-8b45-396f-22886b3a7b1c.png","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/072f49b1-ad9f-8b45-396f-22886b3a7b1c.thumb.png"}]}
         });
         });
      </script>
   </head>
   <body id="template" class="background-custom">
      <div class="site-frame">
         <div id="wrapper" class="container-content external-border3-black ">
            <!--<div class="external-top"><div><div><div><div></div></div></div></div></div>-->
            <div class="external-side-left">
               <div class="external-side-left-top">
                  <div class="external-side-left-bottom">
                     <div class="external-side-right">
                        <div class="external-side-right-top">
                           <div class="external-side-right-bottom">
                              <div id="watermark" class="pageContentText background23-white background border-none">
                                 <div id="watermark-content" class="container-content">
                                    <div id="layout">
                                       <div id="header" class="header border-none">
                                          <div id="header-top" class="top">
                                             <div>
                                                <div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="header-side" class="side">
                                             <div id="header-side2" class="side2">
                                                <div class="container-content">
                                                   <div id="header-content">
                                                      <div class="container-content-inner" id="header-content-inner">
                                                         <!--<div class="widget widget-text" id="widget-fdc8ed2e-80aa-dad9-32a1-1f9e25f1a2ee">
                                                            <div class="widget-content"><p><img id="mce-6084" style="margin-right: auto; margin-left: auto; display: block;" src="attachments/Image/topheader_1.png" alt="" width="1024" height="10" /></p></div>
                                                            </div>-->
                                                         <div class="widget widget-header" id="widget-9f0de22c-309d-01e2-c041-eb8e0648ccc6">
                                                            <div class="widget-content">
                                                               <a href="home"><img src="attachments/Header/finallogo.png" class="header-image5" alt="" style="width:220px;height:94px;"></a>
                                                               <img src="admin/assets/img/profile/<?php echo isset($profile['image'])?$profile['image']:"image"; ?>" class="header-image1" alt="" style="width:140px;height:110px;">
                                                            <div class="header-image2">
                                                               <b> <?php echo isset($profile['heading'])?$profile['heading']:"heading"; ?></b>
                                                               <p> <?php echo isset($profile['name'])?$profile['name']:"name"; ?> <br> Mobile no. <?php echo isset($profile['mobile'])?$profile['mobile']:"mobile"; ?></p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="widget widget-navigation" id="widget-617ca861-db53-45a1-27f0-a036f4a59f85">
                                                            <div class="widget-content">
                                                               <ul class="navigation" id="navigation-617ca861-db53-45a1-27f0-a036f4a59f85">
                                                                  <li class="selected">
                                                                     <a href="home">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Home</span>
                                                                     </a>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="about-lic">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">About LIC</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class="">
                                                                           <a href="history">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">History</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="objective">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Objective</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://www.licindia.in/Media/Awards.aspx">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Awards</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="opreations">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Operations</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="subsidiaries">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Subsidiaries</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="performance">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Performance</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="lic-act-1956">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">LIC Act 1956</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="about-us">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">About Us</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class="">
                                                                           <a href="our-vision">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Our Vision</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="our-mission">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Our Mission</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="our-team">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Our Team</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="infopage">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">InfoPage</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class="">
                                                                           <a href="tax-planning">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Tax Planning</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Customer-Services/Bonus-Information">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Bonus Information</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Customer-Services/Phone-Help-Line">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">LIC Helpline Numbers</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="financial-planning">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Financial Planning</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class="">
                                                                           <a href="life-cycle">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Life Cycle</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="page">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Why</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="when">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">When</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="how">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">How</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="taxation">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">TAXATION</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="#">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Lic Products</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class=" navigation-item-expand">
                                                                  <a href="#">
                                                                  <span class="navigation-item-bullet">&gt;</span>
                                                                  <span class="navigation-item-text">New Policies</span>
                                                                  </a>
                                                                     <ul>
                                                                     <?php 
                                                                        $sql = "SELECT * FROM licproduct";
                                                                        $result = $conn->query($sql);
                                                                        if($result->num_rows > 0)
                                                                        {
                                                                           while($licproduct = $result->fetch_assoc())
                                                                           {
                                                                              ?>
                                                                              <li class="">
                                                                              <a href="<?php echo $licproduct['link']; ?>">
                                                                              <span class="navigation-item-bullet">&gt;</span>
                                                                              <span class="navigation-item-text"><?php echo $licproduct['name']; ?></span>
                                                                              </a>
                                                                           </li>
                                                                           <?php
                                                                           }
                                                                        }
                                                                      ?>
                                                                      </ul>
                                                                      </li>
                                                                     <li class="">
                                                                        <a href="http://licindia.in/Products/Aam-Aadmi-Bima-Yojana">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">Aam Aadmi Bima Yojna</span>
                                                                        </a>
                                                                     </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Insurance-Plan">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Insurance Plan</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Special-Plans">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Special Plans</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Pension-Plans">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Pension Plan</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Unit-Plans">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Unit Plans</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Micro-Insurance-Plans">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Micro Insurance Plans</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Withdrawn-Plans">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Withdrawn Plans</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Products/Health-Plans">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Health plans</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="#">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Services</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class="">
                                                                           <a href="http://www.licindia.in/Customer-Services/Tax-Benefit">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Tax-Benefit</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Home-(1)/LICOnlineServicePortal">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Get Policy Status</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://www.licindia.in/Customer-Services/Premium-Calculator.aspx">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Premium Calculator</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="contact">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Premium Payments</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="faqs">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">FAQs</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="http://licindia.in/Bottom-Links/Download-Forms">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Download Form</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class=" navigation-item-expand">
                                                                     <a href="#" >
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Utilities</span>
                                                                     </a>
                                                                     <ul>
                                                                        <li class="">
                                                                           <a href="attachments/gscop.pdf" target="_blank">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">About GS Cop</span>
                                                                           </a>
                                                                        </li>
                                                                        <li class="">
                                                                           <a href="our-product">
                                                                           <span class="navigation-item-bullet">&gt;</span>
                                                                           <span class="navigation-item-text">Products</span>
                                                                           </a>
                                                                        </li>
                                                                     </ul>
                                                                  </li>
                                                                  <li class="">
                                                                     <a href="achievements">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Achievements</span>
                                                                     </a>
                                                                  </li>
                                                                  <li class="">
                                                                     <a href="contact">
                                                                     <span class="navigation-item-bullet">&gt;</span>
                                                                     <span class="navigation-item-text">Contact Us</span>
                                                                     </a>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="header-bottom" class="bottom">
                                             <div>
                                                <div></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="columns">
                                          <!--form-->
                                          <div class="main-container fadeIn animated">
                                             <div class="container contactgap">
                                                <div class="row">
                                                   <div class="col-md-7 col-sm-12 col-form">
                                                      <h2 class="title-style-2">CONTACT FORM <span class="title-under"></span></h2>
                                                      <form action="querymail.php" class="contact-form ajax-form" method="post">
                                                         <div class="row">
                                                            <div class="form-group col-md-6">
                                                               <input type="text" name="name" class="form-control" placeholder="Name*" required="">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                               <input type="email" name="email" class="form-control" placeholder="E-mail*" required="">
                                                            </div>
                                                         </div>
                                                         <div class="form-group">
                                                            <textarea name="message" rows="5" class="form-control" placeholder="Message*" required=""></textarea>
                                                         </div>
                                                         <div class="form-group alerts">
                                                            <div class="alert alert-success" role="alert">
                                                            </div>
                                                            <div class="alert alert-danger" role="alert">
                                                            </div>
                                                         </div>
                                                         <div class="form-group">
                                                            <button type="submit" class="btn btn-primary pull-right">Send message</button>
                                                         </div>
                                                         <div class="clearfix"></div>
                                                      </form>
                                                   </div>
                                                   <div class="col-md-4 col-md-offset-1 col-contact">
                                                      <h2 class="title-style-2"> MY CONTACT <span class="title-under"></span></h2>
                                                      <p>
                                                         <b>Sanjay Sharma</b><br> "we are the best in insurance market we do not need to proof about our company we sell good products to buyers which helps to safe family with and without you".
                                                      </p>
                                                      <div class="contact-items">
                                                         <ul class="list-unstyled contact-items-list">
                                                            <li class="contact-item"> <span class="contact-icon"> <i class="fa fa-map-marker"></i></span><b>Address 1</b><br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp E-1 JHANDEWALAN EXT , BALUJA HOUSE , &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp DELHI 110055<br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>Address 2</b><br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp G S COMPUTER CONSULTANCY 496 , &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp UDYOG KENDRA II , ECOTECH III,<br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp GREATER NOIDA<br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>BRANCH LOCATION</b><br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp BRANCH 113 , JEEVAN TARA BUILDING ,<br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp1ST FLOOR GATE NO 4-5, SANSAD MARG ,<br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp NEW DELHI 110001
                                                            </li>
                                                            <li class="contact-item"> <span class="contact-icon"> <i class="fa fa-phone"></i></span>9891478002,9212221506,9891478007,<br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp011-41540139/23593313</li>
                                                            <li class="contact-item"> <span class="contact-icon"> <i class="fa fa-envelope"></i></span> gsfcconline@gmail.com, &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsplicindiancr@gmail.com</li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                                <!-- /.row -->
                                             </div>
                                          </div>
                                          <!--end form-->
                                          <!--map-->
                                          <div id="map" style="width:100%;height:350px;background:#ccc"></div>
                                          <script>
                                             function initMap() {

	var broadway = {
		info: '<strong>Licindiancr</strong><br>\
					E-1 JHANDEWALAN EXT,BALUJA HOUSE,DELHI 110055<br> DELHI 110055<br>\
					',
		lat: 28.645116,
		long: 77.204221
	};

	var belmont = {
		info: '<strong>Licindiancr</strong><br>\
					G S COMPUTER CONSULTANCY 496 ,UDYOG KENDRA II , ECOTECH III,<br> GREATER NOIDA,India<br>\
					',
		lat: 28.533243,
		long: 77.469093
	};

	

	var locations = [
      [broadway.info, broadway.lat, broadway.long, 0],
      [belmont.info, belmont.lat, belmont.long, 1],
      
    ];

	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 10,
		center: new google.maps.LatLng(28.620764, 77.363929),
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	var infowindow = new google.maps.InfoWindow({});

	var marker, i;

	for (i = 0; i < locations.length; i++) {
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			map: map
		});

		google.maps.event.addListener(marker, 'click', (function (marker, i) {
			return function () {
				infowindow.setContent(locations[i][0]);
				infowindow.open(map, marker);
			}
		})(marker, i));
	}
}

                                          </script>
                                          <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8qngVObWhwvfsIOivE7eZjXwUJ9Qq_Xk&callback=initMap"></script>
                                          <!--end map-->
                                       </div>
                                       <!--google map-->
                                       <!--<div id="contact-map" class="contact-map">-->
                                       <!--</div>-->
                                       <!--end google map-->
                                    </div>
                                 </div>
                                 <div id="footer-wrapper">
                                    <div id="footer" class="footer border-none">
                                       <div id="footer-top" class="top">
                                          <div>
                                             <div></div>
                                          </div>
                                       </div>
                                       <div id="footer-side" class="side">
                                          <div id="footer-side2" class="side2">
                                             <div class="container-content">
                                                <div id="footer-content">
                                                   <div class="container-content-inner" id="footer-content-inner">
                                                      <div class="widget widget-text" id="widget-73fdf89a-3b66-ffa8-7965-cab511a338e3">
                                                         <div class="widget-content">
                                                            <p style="text-align: center;">&copy; 1989-2017. LIC India NCR . All Rights Reserved.</p>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="footer-bottom" class="bottom">
                                          <div>
                                             <div></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="external-bottom">
            <div>
               <div>
                  <div>
                     <div></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <script type="text/javascript" src="js/anti_cache.js"></script>
      <script type="text/javascript">
         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', 'UA-34705881-1']);
         _gaq.push(['_trackPageview']);
         
         (function() {
           var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
           ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
           var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();
         
      </script>
      <script src="js/main.js"></script>
      <script src="assets/plugins/jquery-1.10.2.js"></script>
      <!--<script src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyC7UFnDJ-32-DnO-VLZgZaDrv70CiCPKDo&amp;libraries=places" type="text/javascript"></script>-->
      <!-- BOOTSTRAP SCRIPTS  -->
      <script src="assets/plugins/bootstrap.js"></script>
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>
   </body>
</html>