//Function To Display Popup
function div_show(id,eid) {
	if(id == 'updateProduct')
	{
		$("#updatename").val($("#"+eid+" .name").html());
		$("#updatedetails").val($("#"+eid+" .details").html());
		$("#updateprice").val($("#"+eid+" .price").html());
		$("#updatedate").val($("#"+eid+" .date").html());
		$("#updateId").val(eid);
	}
	if(id == 'updateTeam')
	{
		$("#updatename").val($("#"+eid+" .name").html());
		$("#updateclia").val($("#"+eid+" .clia").html());
		$("#updateaddress").val($("#"+eid+" .location").html());
		$("#updateuid").val($("#"+eid+" .uid").html());
		$("#updatecontact").val($("#"+eid+" .contact").html());
		$("#updateId").val(eid);
	}
	if(id == 'updateNews')
	{
		$("#updatetitle").val($("#"+eid+" .title").html());
		$("#updatedescription").val($("#"+eid+" .description").html());
		$("#updatelink").val($("#"+eid+" .link").html());
		$("#updateId").val(eid);
	}
	if(id=='updateProfile')
	{
		$("#updatename").val($("#name").html());
		$("#updateemail").val($("#email").html());
		$("#updatemobile").val($("#mobile").html());
		$("#updateheading").val($("#heading").html());
	}
	if(id =='updateLicproduct')
	{
		console.log(id);
		$("#updatename").val($("#"+eid+" .name").html());
		$("#updatelink").val($("#"+eid+" .link").html());
		$("#updateId").val(eid);
	}
	if (id=='imageview')
	{
		$("#imageviewer").attr('src', eid);
	}
	if (id == 'updateImage')
	{
		$("#fetchimage").val(eid);
	}
	$("#"+id).css('display', 'block');
	$("#deleteId").val(eid);
}
//Function to Hide Popup
function div_hide(id){
$("#"+id).css('display', 'none');
// document.getElementById('addProduct').style.display = "none";
}
$(document).ready(function(){
		var i= 0;  
     $('#searchfor').keyup(function(){
     	var searchedText = $(this).val();
     	 $(".name").each(function(){
            // If the listed item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(searchedText, "i")) < 0)
            {
                $(this).parent().fadeOut();
            // Show the listed item if the phrase matches and increase the count by 1
            }
            else
            {
                $(this).parent().show();    
            }
        });
     	 $(".title").each(function(){
            // If the listed item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(searchedText, "i")) < 0)
            {
                $(this).parent().fadeOut();
            // Show the listed item if the phrase matches and increase the count by 1
            }
            else
            {
                $(this).parent().show();    
            }
        });
    });
});

$(document).ready(function(){
	if ($("#message").is(':empty'))
	{
		$("#message").hide();
	}
	else
	{
		$("#message").fadeIn();
		$("#message").fadeOut(5000);
	}
});
$( document ).ready(function(){
	$("#mailPassword").click(function(){
		email = $('#email').val();
		$.ajax({
				type: "POST",
				url: 'mail.php',
				data:{"email" : email},
				success: function(data)
				{
					$("#mailresponse").fadeIn();
					$("#mailresponse").html(data);
					$("#mailresponse").fadeOut(10000);
					$("#email").val("");
				}
		});
	});	
});
$(document).ready(function() {
	for (var i = 0; i <$(".galleryimage").length; i++)
	{
		if(i>0)
		{
			$('.galleryimage').eq(i).css('display', 'none');
		}
	}
	$("#imageright").click(function() {
		for (var i = 0; i <$(".galleryimage").length; i++)
		{
			if($('.galleryimage').eq(i).css('display') == 'inline')
			{
				var j = i;
			}
		}
		$('.galleryimage').eq(j).css('display', 'none');
		if(j == $(".galleryimage").length-1)
		{
			$('.galleryimage').eq(0).css('display', 'inline');
		}
		else
		{
			$('.galleryimage').eq(j+1).css('display', 'inline');
		}
	});
	$("#imageleft").click(function() {
		for (var i = 0; i <$(".galleryimage").length; i++)
		{
			if($('.galleryimage').eq(i).css('display') == 'inline')
			{
				var j = i;
			}
		}
		$('.galleryimage').eq(j).css('display', 'none');
		if(j == 0)
		{
			$('.galleryimage').eq($(".galleryimage").length-1).css('display', 'inline');
		}
		else
		{
			$('.galleryimage').eq(j-1).css('display', 'inline');
		}
	});

});