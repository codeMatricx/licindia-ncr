 <?php 
 function test_input($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
if (isset($_POST['addTeam'])) {
    $name = test_input($_POST["name"]);
    $clia = test_input($_POST["clia"]);
    $location = test_input($_POST["location"]);
    $uid = test_input($_POST["uid"]);
    $contact = test_input($_POST["contact"]);
    $status = 1;
    if (empty($name) || empty($clia) || empty($location) || empty($uid) || empty($contact)) {
        $status=0;
    }
    if ($status) {
        $sql = "INSERT INTO team (name,clia,location,uid,contact) VALUES ('$name','$clia','$location','$uid','$contact')";
        if ($conn->query($sql) === TRUE)
        {
           $responseMessage =  "Team Member Add successfully";
        }
        else
        {
            $responseMessage =  "Connection failed: " . $conn->connect_error;
        }
    }
}
if (isset($_POST['deleteTeam']))
{
    $id = test_input($_POST['id']);
    $sql = "DELETE FROM team WHERE id=$id";
    if ($conn->query($sql) === TRUE)
    {
       $responseMessage =  "Team Member Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->connect_error;
    }
}
if (isset($_POST['updateTeam'])) {
    $name = test_input($_POST["name"]);
    $clia = test_input($_POST["clia"]);
    $location = test_input($_POST["location"]);
    $uid = test_input($_POST["uid"]);
    $contact = test_input($_POST["contact"]);
    $id = test_input($_POST['id']);
    $status = 1;
    if (empty($name) || empty($clia) || empty($location) || empty($uid) || empty($contact)) {
        $status=0;
    }
    if ($status) {
        $sql = "UPDATE team SET name = '$name', clia = '$clia', location = '$location', uid = '$uid', contact = '$contact' WHERE id = $id";
        if ($conn->query($sql) === TRUE)
        {
           $responseMessage =  "Member data edit successfully";
        }
        else
        {
            $responseMessage =  "Connection failed: " . $conn->connect_error;
        }
    }
}
  ?>

<div class="inner" style="min-height: 500px;">
    <div class="row">
        <div class="col-lg-12">
            <h2 style="margin-top: 25px;"> Our Team </h2>
            <input type="text" id="searchfor" placeholder="Search Here.." title="Type in a name" style=" position: absolute; width: 191px;left: 700px; margin-top: -36px;">

                <button id="popup" class="btn text-muted text-center btn-success" onclick="div_show('addTeam')" style="width: 90px; margin-top: -49px; margin-left: 980px;">Add New</button>
        </div>
    </div>
    <hr />
     
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="">
                    <div class="table-responsive" style="position: absolute; left: 8px; width: 99%;">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                <th>Serial</th>
                                <th>Name</th>
                                <th>C.L.I.A</th>
                                <th>ADDRESS</th>
                                <th>ID</th>
                                <th>MOBILE NO.</th>
                                <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $sql = "SELECT * from team";
                                $result = $conn->query($sql);
                                if ($result->num_rows>0)
                                {
                                    $serial=1;
                                    while($team = $result->fetch_assoc())
                                    {
                                            
                                ?>
                                <tr class="tosearch" id="<?php  echo $team['id'];?>">
                                    <td style="text-align: center;"><?php echo $serial; ?></td>
                                    <td style="text-align: left;" class="name"><?php  echo $team['name'];?></td>
                                    <td style="text-align: left;" class="clia"><?php  echo $team['clia'];?></td>
                                    <td style="text-align: center;" class="location"><?php  echo $team['location'];?></td>
                                    <td style="text-align: center;" class="uid"><?php  echo $team['uid'];?></td>
                                    <td style="text-align: center;" class="contact"><?php echo $team['contact']; ?></td>
                                    <td style="font-size: 15px; text-align: center">
                                        <a class="<?php  echo $team['id'];?>" onclick="div_show('updateTeam',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">UPDATE</a>/
                                        <a class="<?php  echo $team['id'];?>" onclick="div_show('deleteTeam',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">DELETE</a></td>
                                </tr>
                                <?php
                                    $serial++;
                                     } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="addTeam">
                    <!-- Popup Div Starts Here -->
                    <div id="popupAdd" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('addTeam')">
                        <form  id="form" method="post" name="form">
                            
                            <h2>Add Member</h2>
                            <hr>
                            <input id="name" name="name" placeholder=" Member Name" type="text">
                            <input id="clia" name="clia" placeholder="C.L.I.A" type="text">
                            <input id="address" name="location" placeholder="Address" type="text">
                            <input id="uid" name="uid" placeholder="ID" type="text">
                            <input id="contact" name="contact" placeholder="Mobile Number" type="text">
                            <input type="submit" id="submit" name="addTeam" value="Add">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <div id="updateTeam">
                    <!-- Popup Div Starts Here -->
                    <div id="popupUpdate" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('updateTeam')">
                        <form  id="form" method="post" name="form">
                            
                            <h2>Update Member's Detail</h2>
                            <hr>
                            <input id="updatename" name="name" placeholder=" Member Name" type="text">
                            <input id="updateclia" name="clia" placeholder="C.L.I.A" type="text">
                            <input id="updateaddress" name="location" placeholder="Address" type="text">
                            <input id="updateuid" name="uid" placeholder="ID" type="text">
                            <input id="updatecontact" name="contact" placeholder="Mobile Number" type="text">
                            <input id="updateId" type="hidden" name="id">
                            <input type="submit" id="submit" name="updateTeam" value="Update">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                 <!-- Display Popup Button -->
                <div id="deleteTeam">
                    <!-- Popup Div Starts Here -->
                    <div id="popupDelete" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('deleteTeam')">
                        <form method="post">
                            <hr>
                            <h2>Are You Sure??</h2>
                            <input type="submit" name="deleteTeam" value="OK">
                            <input type="hidden" name="id" id="deleteId">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <!--POP-->
            </div>
        </div>
    </div>
</div>